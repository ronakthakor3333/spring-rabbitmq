package com.rabbit.rabbitbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitBeApplication.class, args);
	}

}
