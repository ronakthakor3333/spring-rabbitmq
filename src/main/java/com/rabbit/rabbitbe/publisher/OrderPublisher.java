package com.rabbit.rabbitbe.publisher;

import com.rabbit.rabbitbe.config.MessagingConfig;
import com.rabbit.rabbitbe.dto.Order;
import com.rabbit.rabbitbe.dto.OrderStatus;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/order")
public class OrderPublisher {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping("/book/{restaurantName}")
    public String bookOrder(@RequestBody Order order, @PathVariable String restaurantName) {
        order.setOrderId(UUID.randomUUID().toString()); //from order service
//        request will go the restaurant service

//        then request will go to the payment service

        OrderStatus orderStatus = new OrderStatus(order, "In process", "Order placed succesfully in " + restaurantName);

        rabbitTemplate.convertAndSend(MessagingConfig.topicExchange, MessagingConfig.bindingKey, orderStatus);

        return "success";
    }

}
