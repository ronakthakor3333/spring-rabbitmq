package com.rabbit.rabbitbe.consumer;

import com.rabbit.rabbitbe.config.MessagingConfig;
import com.rabbit.rabbitbe.dto.OrderStatus;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class User {

    @RabbitListener(queues = MessagingConfig.QUEUE)
    public void cosumeMessageFromQueue(OrderStatus orderStatus) {
        System.out.println("Message received from queue :" + orderStatus);
    }


}
